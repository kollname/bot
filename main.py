import random
import time
import check_tasks
import threading
import datetime
from vk_api.longpoll import VkLongPoll, VkEventType
import vk_api
import task
import get_one_picture
import spelling_сhecker
import get_pictures
import re

global current_user




vk_session = vk_api.VkApi(token='')
current_user = 1
messageCounter = 0
key_words = {"key_word": []}
list_of_tasks = []

# Each user has own combination. The combination depends on business requirements
users_logic = {current_user: {'user_id': '', 'второй_вопрос': '', 'приветствие': ''}, current_user + 1: 'n'}
session_api = vk_session.get_api()
longpoll = VkLongPoll(vk_session)

# 174606397 - 1st
# 160475256 - no
# 162406982 - yes
try:
    attachment = get_one_picture.get(vk_session, -174606397, session_api)  # first photo/ 1 message
    attachment_two = get_one_picture.get(vk_session, -160475256, session_api)  # negative and neutral answer
    attachment_three = get_pictures.get(vk_session, -162406982, session_api)  # positive answer
except:
    pass
unfamiliar = True



while True:
    try:
        for event in longpoll.listen():
            if list_of_tasks:
                check_tasks.check(list_of_tasks)
            if event.type == VkEventType.MESSAGE_NEW:
                response = event.text.lower()
                #     sender_name = session_api.users.get(peer_id = event.peer_id)[0]
                if event.from_user and not event.from_me:
                    oldBoy = False
                    sender_name = session_api.users.get(user_ids=event.peer_id)[0]
                    current_user = event.user_id
                    for u in users_logic.keys():

                        if u == current_user:
                            oldBoy = True
                            break

                    if not oldBoy:
                        users_logic[current_user] = {'user_id': str(event.user_id), 'второй_вопрос': 'нет',
                                                     'приветствие': 'пусто'}

                    # print(sender_name['first_name'], sender_name["last_name"])

                    # Split whole sentence into separate words

                    responsesplited = re.findall(r"\w+|[^\w\s]", response, re.UNICODE)

                    # check every word recieved
                    for i in range(len(responsesplited)):
                        key_words['key_word'].append(spelling_сhecker.spellсheck(responsesplited[i]))
                    if users_logic[current_user]['приветствие'] == 'пусто':
                        message = 'Здравстуйте, {0}. ' \
                                  'Мы хотим пригласить вас на первый Курский фестиваль гастрономического искусства и ' \
                                  'крафтовых напитков *gre4afest ("ГРЕЧА")\n' \
                                  '✅ Море эмоций \n' \
                                  '✅ Вкуснейшие напитки \n' \
                                  '✅ Выступления музыкальных коллективов. \n' \
                                  'Такого в нашем городе еще не было! ' \
                                  'Всю информацию можно посмотреть тут—» https://vk.com/gre4afest  или на нашем сайте \n' \
                                  'https://гречафест.рф \n' \
                                  'Скажите Вы любите крафтовое пиво и медовуху? '
                        a = datetime.datetime.now()
                        b = a + datetime.timedelta(0, 30)
                        list_of_tasks.append(task.Task(vk_session, message, b, sender_name['first_name'], str(event.user_id), attachment, 1))

                        users_logic[current_user] = {'user_id': str(event.user_id), 'второй_вопрос': 'открыт',
                                                     'приветствие': 'было'}
                        for l in range(3):
                            for k in key_words["key_word"]:
                                key_words['key_word'].remove(k)
                        break

                    if users_logic[current_user]['приветствие'] == 'закрыт' and key_words['key_word'] != 0 and \
                            users_logic[current_user]['второй_вопрос'] == 'закрыт' and key_words['key_word'].count(
                        'заново'):
                        message = 'Бот обновлен'
                        a = datetime.datetime.now()
                        list_of_tasks.append(task.Task(vk_session, message, a, "", str(event.user_id), "", 3))

                        users_logic.clear()

                    if users_logic[current_user]['второй_вопрос'] == 'открыт' and key_words['key_word'] != 0:
                        if (key_words['key_word'].count('нет') or
                                (key_words['key_word'].count('не') and key_words['key_word'].count('думаю')) or
                                (key_words['key_word'].count('не') and key_words['key_word'].count('интересно') and not
                                key_words['key_word'].count('но')) or
                                (key_words['key_word'].count('не') and key_words['key_word'].count('смогу')) or
                                (key_words['key_word'].count('не') and key_words['key_word'].count('пью')) or
                                (key_words['key_word'].count('не') and key_words['key_word'].count('употребляю')) or
                                (key_words['key_word'].count('другие') and key_words['key_word'].count('планы')) or
                                key_words['key_word'].count('отказ') or
                                key_words['key_word'].count('воздержусь') or
                                key_words['key_word'].count('зож') or
                                key_words['key_word'].count('трезвенник')):

                            message = 'Очень жаль, возможно вам просто не довелось попробовать лучшие крафтовые напитки, ' \
                                      'которые привезут наши участники со всех уголков нашей огромной  страны.Кроме того ' \
                                      'на фестивале можно будет замечательно провести время, послушать известные музыкальные ' \
                                      'коллективы и посмотреть за творчеством разнообразных артистов, извините за беспокойство...'
                            a = datetime.datetime.now()
                            b = a + datetime.timedelta(0, 25)

                            list_of_tasks.append(task.Task(vk_session, message, b, "", str(event.user_id), attachment_two, 2))

                            users_logic[current_user] = {'user_id': str(event.user_id), 'второй_вопрос': 'закрыт',
                                                         'приветствие': 'закрыт'}

                            for l in range(3):
                                for k in key_words["key_word"]:
                                    key_words['key_word'].remove(k)  # Ответ: Не сейчас

                        elif (key_words['key_word'].count('потом') or
                              key_words['key_word'].count('понятно') or
                              key_words['key_word'].count('интересно') or
                              key_words['key_word'].count('подумаю') or
                              key_words['key_word'].count('позже') or
                              key_words['key_word'].count('заранее') or
                              key_words['key_word'].count('возможно') or
                              key_words['key_word'].count('рано') or
                              (key_words['key_word'].count('на') and key_words['key_word'].count('будующее')) or
                              (key_words['key_word'].count('интересно') and key_words['key_word'].count('но')) or
                              (key_words['key_word'].count('не') and key_words['key_word'].count('пробовал')) or
                              (key_words['key_word'].count('не') and key_words['key_word'].count('особо')) or
                              (key_words['key_word'].count('беру') and key_words['key_word'].count('заметку')) or
                              (key_words['key_word'].count('иметь') and key_words['key_word'].count('ввиду')) or
                              (key_words['key_word'].count('видно') and key_words['key_word'].count('будет')) or
                              (key_words['key_word'].count('возникнет') and key_words['key_word'].count(
                                  'необходимость')) or
                              (key_words['key_word'].count('не') and key_words['key_word'].count('до') and key_words[
                                  'key_word'].count('этого')) or
                              (key_words['key_word'].count('"интересно",') and key_words['key_word'].count(
                                  'будующее'))):

                            message = 'У нас вы сможете попробовать лучшие крафтовые напитки, ' \
                                      'которые привезут наши участники со всех уголков нашей огромной  ' \
                                      'страны. Кроме того на фестивале можно будет замечательно провести время, ' \
                                      'послушать известные музыкальные коллективы и посмотреть за творчеством ' \
                                      'разнообразных артистов, приходите мы вас ждем   🍺😊!'
                            a = datetime.datetime.now()
                            b = a + datetime.timedelta(0, 30)
                            list_of_tasks.append(task.Task(vk_session, message, b, "", str(event.user_id), attachment_two, 2))

                            users_logic[current_user] = {'user_id': str(event.user_id), 'второй_вопрос': 'закрыт',
                                                         'приветствие': 'закрыт'}
                            for l in range(3):
                                for k in key_words["key_word"]:
                                    key_words['key_word'].remove(k)

                        else:
                            message = 'Наше мероприятие  пройдет 12-13 июня \n по адресу Димитрова 66! \n' \
                                      'Вы сможете попробовать более 240 видов крафтового пива и медовухи, получить на входе \n' \
                                      'бокал фестиваля с жетонами. \n' \
                                      'Стоимость билета на пятницу 700₽, на субботу 1000₽. \n' \
                                      ' ❗ ВЫГОДНО ❗\n' \
                                      'Билет на два дня до марта месяца ВСЕГО 1300₽ \n' \
                                      'Цены действуют на первые 300 билетов. \n' \
                                      'Приобретайте билеты в наших группе - *gre4afest ("ГРЕЧА") \nили на нашем сайте - ' \
                                      'https://гречафест.рф \n' \
                                      'Приходите будем Вас ждать!'
                            a = datetime.datetime.now()
                            b = a + datetime.timedelta(0, 10)

                            list_of_tasks.append(task.Task(vk_session, message, b, "", str(event.user_id), attachment_three, 2))

                            users_logic[current_user] = {'user_id': str(event.user_id), 'второй_вопрос': 'закрыт',
                                                         'приветствие': 'закрыт'}
                            for l in range(3):
                                for k in key_words["key_word"]:
                                    key_words['key_word'].remove(k)

                messageCounter += 1
                if messageCounter > 1000:
                    messageCounter = 0
                    users_logic.clear()

                    # ВСЕ что закоментированно дальше это. Если мы хотим, чтобы бот дальше продолжал общаться с клиентом и отаравлял эти сообщения организатору.

                    # if users_logic[current_user]['приветствие'] == 'закрыт' and key_words['key_word'] != 0 and \
                    #         users_logic[current_user]['второй_вопрос'] == 'закрыт':
                    #     vk_session.method('messages.send',
                    #                       {'user_id': event.user_id,
                    #                        'message': 'За всеми вопросами обращайтесь к организатору нашего мероприятия. '
                    #                                   'Хотите, чтобы он с вами связался?',
                    #                        'random_id': 0})
                    #     users_logic[current_user] = {'user_id': str(event.user_id), 'второй_вопрос': 'ожидает_ответа',
                    #                                  'приветствие': 'закрыт'}
                    #
                    #     for l in range(3):
                    #         for k in key_words["key_word"]:
                    #             key_words['key_word'].remove(k)  # Ответ: Нет
                    #     break
                    #
                    # if users_logic[current_user]['второй_вопрос'] == 'ожидает_ответа' and key_words['key_word'] != 0:
                    #     if (key_words['key_word'].count('да') or
                    #             key_words['key_word'].count('хочу')):
                    #         vk_session.method('messages.send',
                    #                           {'user_id': 31429457,
                    #                            'message': 'Здравствуйте Господин - этот человек требует вашей помощи: vk.com/id{0}  Мероприятие медовуха ;))'.format(
                    #                                event.user_id),
                    #                            'random_id': 0})
                    #         vk_session.method('messages.send',
                    #                           {'user_id': event.user_id,
                    #                            'message': 'Хорошо, ожидайте вам обязательно напишут ;))',
                    #                            'random_id': 0})
                    #
                    #         users_logic[current_user] = {'user_id': str(event.user_id),
                    #                                      'второй_вопрос': 'закрыт',
                    #                                      'приветствие': 'закрыт'}
                    #         for l in range(3):
                    #             for k in key_words["key_word"]:
                    #                 key_words['key_word'].remove(k)  # Ответ: Нет
                    #         break
                    #     else:
                    #         vk_session.method('messages.send',
                    #                           {'user_id': event.user_id,
                    #                            'message': 'Добро!',
                    #                            'random_id': 0})
                    #         users_logic[current_user] = {'user_id': str(event.user_id),
                    #                                      'второй_вопрос': 'закрыт',
                    #                                      'приветствие': 'закрыт'}
                    #
                    #         for l in range(3):
                    #             for k in key_words["key_word"]:
                    #                 key_words['key_word'].remove(k)  # Ответ: Нет
                    #         break



    except:
        pass
