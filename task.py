import datetime
class Task:
    # type 1 - приветствие. (имя кастомера + картинка)
    # type 2 - обычное сообщение с картинкой
    # type 3 - сообщение без картинки
    def __init__(self, vk_session, text, timeOfExecution, customerName, customerId, attachment, typeOftask):
        if(typeOftask == 1):
            self.typeOftask = typeOftask
            self.vk_session = vk_session
            self.attachment = attachment
            self.text = text
            self.timeOfExecution = timeOfExecution
            self.customerName = customerName
            self.customerId = customerId
            self.executed = False
        elif typeOftask == 2:
            self.typeOftask = typeOftask
            self.vk_session = vk_session
            self.attachment = attachment
            self.text = text
            self.timeOfExecution = timeOfExecution
            self.customerName = ""
            self.customerId = customerId
            self.executed = False
        else:
            self.typeOftask = typeOftask
            self.vk_session = vk_session
            self.attachment = ""
            self.text = text
            self.timeOfExecution = timeOfExecution
            self.customerName = ""
            self.customerId = customerId
            self.executed = False

    def execute(self):
        if(self.typeOftask == 1):
            self.executeTaskWithName()
        elif(self.typeOftask == 2):
            self.executeTaskWithAttachment()
        else:
            self.executeTask()

    def executeTaskWithName(self):
        self.vk_session.method('messages.send',
                                          {'user_id': self.customerId, 'message': self.text.format(
                                              self.customerName),
                                           'random_id': 0, "attachment": self.attachment})
        self.executed = True


    def executeTaskWithAttachment(self):
        self.vk_session.method('messages.send',
                                          {'user_id': self.customerId, 'message': self.text,
                                           'random_id': 0, "attachment": self.attachment})
        self.executed = True

    def executeTask(self):
        self.vk_session.method('messages.send',
                                          {'user_id': self.customerId, 'message': self.text,
                                           'random_id': 0})
        self.executed = True
