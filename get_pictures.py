import random

def get(vk_session, id_group, vk):
    try:
        attachment = ''

        max_num = vk.photos.get(owner_id=id_group, album_id='wall', count=0)['count']

        num = 2
        pictures = vk.photos.get(owner_id=str(id_group), album_id='wall', count=1, offset=num)['items']
        buf = []
        for element in pictures:
            buf.append('photo' + str(id_group) + '_' + str(element['id']))
        attachment = ','.join(buf)

        return attachment
    except:
        return get(vk_session, id_group, vk)