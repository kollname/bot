import datetime
def check(list_of_tasks):
    for task_object in list_of_tasks:
        if datetime.datetime.now() >= task_object.timeOfExecution:
            task_object.execute()
        if task_object.executed:
            list_of_tasks.remove(task_object)